<?php
/**
 * Created by PhpStorm.
 * User: Escritorio
 * Date: 09/11/2018
 * Time: 23:03
 */


if (isset($_POST['submit'])) {
    echo "<pre>";
    echo "file received\n";
    print_r($_FILES['file_upload']);

    $upload_errorS = array(

        UPLOAD_ERR_OK           => "There is no error",
        UPLOAD_ERR_INI_SIZE     => "The upload file exceeds the upload_max_filesize directory",
        UPLOAD_ERR_FORM_SIZE    => "The upload file exceeds the MAX_FILE_SIZE directory",
        UPLOAD_ERR_PARTIAL      => "The upload file was only partial uploaded",
        UPLOAD_ERR_NO_FILE      => "No file uploaded.",
        UPLOAD_ERR_NO_TMP_DIR   => "Missing a temporary folder.",
        UPLOAD_ERR_CANT_WRITE   => "Failed to write file to disk.",
        UPLOAD_ERR_EXTENSION    => "A PHP extension stopped the file upload."

        );

    // moving the file from temp directory
    $temp_name = $_FILES['file_upload']['tmp_name'];
    $the_file = $_FILES['file_upload']['name'];
    $directory = "uploads";

    if(move_uploaded_file($temp_name, $directory."/". $the_file)){

        $the_message = " File Uploaded successfully.";
    }else{

        $the_error = $_FILES['file_upload']['error'];
        $the_message = $upload_errorS[$the_error];
    }

    echo "<pre>";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>

<form action="upload.php" enctype="multipart/form-data" method="post">

    <?php if (!empty($the_error)) {
        echo $the_message;
    }
    ?>

    <input type="file" name="file_upload"><br>

    <input type="submit" name="submit">
</form>


</body>
</html>
