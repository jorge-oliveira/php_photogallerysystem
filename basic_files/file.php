<?php
/**
 * Created by PhpStorm.
 * User: Escritorio
 * Date: 09/11/2018
 * Time: 22:44
 */

// gets the path and name of file
echo "Path and name of file: " . __FILE__ . "<br>";

// gets the line number
echo "line number: " . __LINE__ . "<br>";

// gets the directory
echo "directory: " . __DIR__ . "<br>";

// check if file exists on the diretory
if (file_exists(__DIR__)) {
    echo "yes <br>";
}

// check if is a file
if (is_file(__DIR__)) {
    echo "yes <br>";
} else {
    echo "no  <br>";
}


// check if is a file
if (is_dir(__FILE__)) {
    echo "yes <br>";
} else {
    echo "no  <br>";
}


// ternary operator
echo file_exists(__FILE__) ? "yes" : "No";


echo phpinfo();