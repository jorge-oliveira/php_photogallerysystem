<?php
/**
 * Created by PhpStorm.
 * User: Escritorio
 * Date: 08/11/2018
 * Time: 21:12
 */

require_once("new_config.php");

class Database {

    public $connection;

    // open a connection to database right away.
    function __construct() {
        $this->open_db_connection();
    }

    // method to opens the connection
    public function open_db_connection() {

        /*// old version
        $this->connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

        if (mysqli_connect_errno()) {
            die("Database connection failed." . mysqli_error());
        }*/

        // new version (create a object for the connection and pass to the connection variable)
        $this->connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

        if ($this->connection->connect_errno) {
            die("Database connection failed." . $this->connection->connect_error);
        }
    }

    // executes the query
    public function query($sql) {

        // old version
        //$result = mysqli_query($this->connection, $sql);
        //return $result;

        // new version
        $result = $this->connection->query($sql);
        $this->confirm_query($result);
        return $result;
    }

    // confirm the query
    private function confirm_query($result) {
        if (!$result) {
            // old version
            //die("Query Failed.". mysqli_error(($this->connection)));

            // new version
            die("Query Failed." . $this->connection->error);
        }
    }

    // escape strings
    public function escape_string($string) {

        // old version
        //$escape_string = mysqli_real_escape_string($this->connection, $string);

        // new version
        $escape_string = $this->connection->real_escape_string($string);
        return $escape_string;
    }

    public function the_insert_id(){

        //old version
        //return mysqli_insert_id($this->connection);

        // new version
        return $this->connection->insert_id();
    }


}

// instanciate right away
$database = new Database();

