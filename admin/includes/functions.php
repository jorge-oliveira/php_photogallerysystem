<?php
/**
 * Created by PhpStorm.
 * User: Escritorio
 * Date: 08/11/2018
 * Time: 23:25
 */

// auto loaderr the classes that aren't included in application
function classAutoLoader($class) {

    $class = strtolower($class);

    $the_path = "includes/{$class}.php";

    if (is_file($the_path) && (!class_exists($class))) {
        include($the_path);

    } else {
        die("this file name {$class}.php was not found.");
    }
}

// redirect function
function redirect($location){

    header("Location: {$location}");
}


spl_autoload_register('classAutoLoader');