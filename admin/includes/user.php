<?php
/**
 * Created by PhpStorm.
 * User: Escritorio
 * Date: 08/11/2018
 * Time: 22:11
 */

class User {

    public $id;
    public $username;
    public $password;
    public $first_name;
    public $last_name;


    public static function find_all_users() {
        return self::find_this_query("SELECT * FROM users");
    }

    public static function find_user_by_id($id) {

        // fetch the user limit one result
        $the_result_array = self::find_this_query("SELECT * FROM users WHERE id=$id LIMIT 1");

//        if (!empty($the_result_array)) {
//            // grab the first item
//            $first_item = array_shift($the_result_array);
//            return $first_item;
//        } else {
//            return false;
//        }
//        return $found_user;

        return !empty($the_result_array) ? array_shift($the_result_array) : false;
    }

    // executing any query
    public static function find_this_query($sql) {
        global $database;
        $the_result_array = $database->query($sql);

        // create a empty array to store the data
        $the_object_array = array();

        // the result is put on the array
        while ($row = mysqli_fetch_array($the_result_array)) {
            $the_object_array[] = self::instantiation($row);
        }

        return $the_object_array;
    }

    public static function verify_user($username, $password) {
        echo "request the log";
        global $database;
        // cleaning the data before enter database
        $username = $database->escape_string($username);
        $password = $database->escape_string($password);

        $query = "SELECT * FROM users WHERE ";
        $query .= "username = '{$username}' ";
        $query .= "AND password = '{$password}' ";
        $query .= "LIMIT 1";

        $the_result_array = self::find_this_query($query);

        return !empty($the_result_array) ? array_shift($the_result_array) : false;
    }

    public static function instantiation($the_record) {

        // instantiate the user object
        $the_object = new self;

        // assign the values
        /*        $the_object->id         = $the_record['id'];
                $the_object->username   = $the_record['username'];
                $the_object->password   = $the_record['password'];
                $the_object->first_name = $the_record['first_name'];
                $the_object->last_name  = $the_record['last_name'];*/

        foreach ($the_record as $the_atribute => $value) {

            // check object has any attribute
            if ($the_object->has_the_attribute($the_atribute)) {
                $the_object->$the_atribute = $value;
            }
        }

        return $the_object;
    }

    private function has_the_attribute($the_attribute) {

        // This function gets the user properties on the top of the given object.
        $object_properties = get_object_vars($this);

        // if the attribute exists return true or false
        return array_key_exists($the_attribute, $object_properties);

    }

}




