<?php
/**
 * Created by PhpStorm.
 * User: Escritorio
 * Date: 09/11/2018
 * Time: 20:11
 */


class Session {

    // properties
    private $signed_in = false;
    public $user_id;
    public $message;

    function __construct() {
        //start a session
        session_start();
        $this->check_the_login();
        $this->check_message();
    }


    // send message
    public function message($msg = "") {
        if (!empty($msg)) {
            $_SESSION['message'] = $msg;
        } else {
            return $this->message;
        }
    }

    // check message already exist and clean
    private function check_message() {
        if (isset($_SESSION['message'])) {
            $this->message = $_SESSION['message'];
            unset($_SESSION['message']);
        } else {
            $this->message = "";
        }
    }


    // getter
    public function is_signed_in() {
        return $this->signed_in;
    }


    // set the login
    public function login($user) {

        if ($user) {

            $this->user_id = $_SESSION['user_id'] = $user->id;
            $this->signed_in = true;
        }
    }

    public function logout() {
        unset($_SESSION['user_id']);
        unset($this->user_id);
        $this->signed_in = false;
    }


    // methods
    private function check_the_login() {

        // check if the session its set
        if (isset($_SESSION['user_id'])) {
            $this->user_id = $_SESSION['user_id'];
            $this->signed_in = true;
        } else {
            unset($this->user_id);
            $this->signed_in = false;
        }
    }


}

// create a instatiation and start a session automatically
$session = new Session();